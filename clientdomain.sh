#!/bin/sh
#
# clientdomain.sh -- Boot the client domain in demo-realm-crossover
#
# From: Rick van Rein <rick@openfortress.nl>


MKROOT=/opt/mkroot/build
SESSION=clientdomain


# Start the link to the external network
#
ifup iwo0pub

# Start a Tmux session to hold the consoles for containers
#
cd $MKROOT
tmux new-session -d -s $SESSION

# Start the public router container (pub0)
#
cd $MKROOT/internetwide/pub2dmz
./net4pub.sh create
tmux new-window -n pub0 -c $PWD 'runc run pub0'

# Start the name resolution node in the LAN service (svc0)
#
cd $MKROOT/internetwide/lanservice
./net4svc.sh create
tmux new-window -n svc0 -c $PWD 'runc run svc0'

# Start the identity provider (id0)
#
cd $MKROOT/internetwide/identity
./net4identity.sh create
tmux new-window -n id0 -c $PWD 'runc run id0'

# Start the KIP server (kip0)
#
cd $MKROOT/internetwide/kip
./net4kip.sh create
tmux new-window -n kip0 -c $PWD 'runc run kip0'

# Start the Mail server (mail0)
#
cd $MKROOT/internetwide/mail
./net4mail.sh create
tmux new-window -n mail0 -c $PWD 'runc run mail0'

# Start the Web server (web0)
#
cd $MKROOT/internetwide/web
./net4web.sh create
tmux new-window -n web0 -c $PWD 'runc run web0'

