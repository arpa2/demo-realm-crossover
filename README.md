# README for demo-realm-crossover

> *This is a demonstration of Realm Crossover.*

Please visit the
[website](http://demo-realm-crossover.arpa2.net)
for information.

To run this site locally, perhaps while writing it, get
[mkdocs](https://www.mkdocs.org/)
and run `mkdocs serve &` to review the
[rendered pages](http://localhost:8000)
on your local machine.  They update when you save.

If you choose to build the images yourself, please follow
the entries on "Reconstructing" in the document index.


## Terminal colours

You may find the white-on-black console difficult to read
on VirtualBox.  You can change that after login, using

```
tput setaf 0 ; tput setab 7 ; clear
```

This setting is temporary, any reset will turn back to
the original colour scheme.
