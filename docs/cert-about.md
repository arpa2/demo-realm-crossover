# About the Certificate plans

> *Certificates bind an identity to a public key, and that key
> can subsequently be used to sign or decrypt in protocols.
> It is also usable for login.*

Certificates may be generated under any domain, with a CA that
signs just for that domain.  To enable domains to trust one
another, DNSSEC and DANE add new possibilities.

For client authentication, a variant on these principles is
required, namely client-side DANE.  This is not currently
standardised.

*This work is not part of these demonstrations yet.*
