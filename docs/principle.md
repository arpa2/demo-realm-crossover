# Principles of Realm Crossover

> *Realm Crossover allows users to take full control over their
> online identity, while using services run by others.*

The result is easiest described to users as Bring Your Own IDentity (BYOID).

## BYOID Usage Pattern

The general usage pattern introduced with InternetWide Identity is one
where a client controls a set of usernames, residing under a realm of its
own choosing.  This client realm is implemented under a client domain name.
The client may approach services running under the same or any foreign
domain.  In either case, the client brings their own identity composed
of the client username and client domain; the client realm actively
facilitates authentication under this composite identity.

As part of client control over their own identity, a service-specific
client username may be selected from among a set of pseudonyms available
to the cient.  This enables the client to manage their identity, and the
client realm can provide a number of forms to facilitate this; clients may
create fresh identities or offer "+alias" extensions, or switch to any
of a group member or role occupant, or even to a shared identity for an
entire group or role.  The client realm or their user agent may remember
choices made in the past and suggest them again during new encounters with
the same service.

The design challenge of InternetWide Identity is to facilitate these
patterns in current-day protocols.  This calls for additional Realm Crossover
protocols and techniques, and the sections below outline how application
protocols with Kerberos and/or
SASL authentication
can be extended to connect client and service realms, usually without
modifications to application-layer protocols.  It also explains how
a distributed Public Key Infrastructure can be relied upon with similar
techniques.

The general pattern of Realm Crossover is founded on the two-level
authority of a user@domain.name identity.  Though host, port and protocol
as well as path and query string may be useful to locate a resource and for
that reason incoporated into a URI, the proposed InternetWide Identity
abstracts from those elements to allow shared identities across a
variation of services and protocols.  Only the domain and the username are
considered for identity.  The foreign service starts by authenticating
the domain and makes an identity provider callback secured with mechanisms
like DNSSEC, DANE and TLS.  The callback should be validated to a point where
its authority over usernames under the domain is certain.  The callback can
then be used, in a manner specific to the Realm Crossover technology, to
authenticate the username underneath its domain.  The composition of
these two elements, username and domain, with an "@" to separate the
fields, forms the full identity as it is further considered by the foreign
service.  This approach can be used for client identities, but may even be
useful to validate service identites.

## New Possibilities

The mere introduction of BYOID enables a number of surprising options
for existing protocols.  Though these are not necessarily useful to all
and certainly not the only new possibilities, they are instructive in
understanding the reach of the proposed model for an InternetWide Identity
with Realm Crossover.

IMAP could be used as a mechanism to share email lists.  This is
already possible for public lists, but not commonly used on account of
the publication of email addresses.  For a limited/internal group, there
are better opportunities of employing this simple paradigm, and granting
internal and external clients access to an email list, with the benefit
of searching with all the facilities that IMAP has to offer.  This does
however require a form of authentication that includes Realm Crossover.
Given that IMAP incorporates SASL authentication, this is possible.

LDAP provides a similar story.  It is the most refined data model that
the IETF has produced.  Public keys can be distributed for PKIX, OpenPGP
and OpenSSH, and this is just one example of technical use cases that can
benefit.  LDAP can also be used for providing or updating contact
information upon request, with a better defined query mechanism than other
specifications.  In all these use cases however, the privacy of the shared
information may be an issue; whether to reveal the information or even
mention its existence can be controlled by an LDAP implementation, but to
that end clients must authenticate.  Since LDAP incorporates SASL
authentication, this form of realm-crossing database access can indeed
be facilitated.

SMTP and AMQP are protocols for passing data between arbitrary users
under arbitrary domains, with a respective focus on human interaction and
automation.  SMTP is weighed down by massive abuse, and AMQP could fall
prey to the same pattern.  The cause for this is unauthenticated access
to an incoming service.  Though authentication of an individual sender
may be overzealous and difficult in operational practice, it is reasonable
to assume that an agent, active on behalf of a client as part of their
realm, can authenticate as a service under the sender's realm.  In case
of indirections (such as email lists or forwarding), there is usually an
option of looking back a few hops with a protocol's Received or Via headers,
at least for known indirections.  In general however, it can add value to
authenticate a sender at the pivotal point where a sending domain's outbound
service connects to a receiving domain's inbound service.  Both SMTP and
AMQP can establish this with SASL.


