> *This is the demonstration material for Realm Crossover.  It allows
> you to play with it with a few VMs that load into VirtualBox.*

Realm Crossover allows a client to authenticate with logic that runs
under their own domain.  Any server anywhere can validate the client
identity as follows:

  * It assures that it connects to the client's domain `client.dom`
  * It retrieves a user identity `clid` for the client in that domain
  * It composes this to `clid@client.dom` as a unique client name

This logic can usually be run within existing protocols, because
they tend to embed standard authentication mechanisms that can
be used with Realm Crossover

  * SASL
  * Kerberos (future work)
  * Certificates (future work)

This work and some other work that is included as a dependency was funded with grants by

  * [NLnet](https://nlnet.nl)
  * [NGI Pointer, an EU program](https://pointer.ngi.eu/ngi-pointer-work-programme/)
  * [SIDNfonds](https://sidnfonds.nl/)

We are grateful for the (continued) support by these funds, they make it
possible for us to invest our time and effort in a radically new approach
to online identity and authentication and authorisation.

