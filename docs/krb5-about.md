# About the Kerberos plans

> *Kerberos is virtually everywhere, if not directly than as
> a SASL mechanism.  It is popular in en enterprise setting
> because it realises single sign-on with short-lived credentials 
> over most protocols.  Of course, hosted identity systems can
> also provide these benefits to users.*

Kerberos already facilitates Realm Crossover, albeit that the
infrastructure was designed before DNSSEC and DANE.  When these
are connected, a very pleasant user flow arises, with only a
single login at the start of a day and silent logins to services
for the remainder of the day.

*This work is not included in this demo yet.*
