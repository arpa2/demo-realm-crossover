# About the SASL demo

> *This demo uses SASL authentication, which is part in many protocols.
> HTTP is deprived of this, and actually has no really good mechanisms
> for authentication, so we introduced SASL to HTTP.  The link between
> domains is made over Diameter.*

## Virtual Machines

The SASL demo consists of three VMs that can be run in VirtualBox,
with a network to connect them.  The network may be inspected to
get an idea of the traffic flow between network nodes.

You will find the VMs in a group named `InternetWide Realm Crossover`:

  * The `Client Desktop` VM runs a desktop with FireFox.  We installed a
    plugin in VM that implements HTTP-SASL when a server offers it.
    This plugin speaks with a client program that pops up a simple
    interaction to provide a password.  HTTP-SASL is an
    [open protocol specification](https://www.ietf.org/id/draft-vanrein-httpauth-sasl-06.html)
    that we wrote.

  * The `Server Domain` VM consists of a number of containers, namely
    a webserver (Apache) that connects to an identity client
    (Diameter) that will connect to the client domain, as requested
    by the SASL mechanism.  The identity client is considered as
    a central node for the container collection; other servers
    could relay SASL inquiries to this node to ask for help with the
    login.  We designed a
    [simple protocol](https://gitlab.com/arpa2/quick-der/-/blob/master/arpa2/Quick-SASL.asn1)
    and even a
    [simple API](http://quick-sasl.arpa2.net/group__quickdiasasl.html)
    with
    [several examples](https://gitlab.com/arpa2/Quick-SASL/-/tree/master/src)
    to use it.  All this should make integration of this flow rather
    simple.

  * The `Client Domain` VM consists of a number of containers,
    predominantly an identity provider (Diameter) that answers
    to the inquiries from the identity client in the server VM.
    The connection uses a SASL embedding in Diameter that we
    specified in an
    [open protocol specification](https://datatracker.ietf.org/doc/draft-vanrein-diameter-sasl/),
    for optimal portability and good network compatibility.

The containers in the server VMs are designed to be part of a
[domain hosting setup](https://gitlab.com/arpa2/mkroot/-/blob/master/doc/setup-internetwide-architecture-hosting.md)
that anyone can run to support their own domain name.
This implements the
[InternetWide Infrastructure](http://internetwide.org/)
that forms the context of this demonstration.

