# Reconstructing the SASL demo

> *The steps to build the SASl demo from scratch, if you do not
> want to use the binary images.  This is what we do.*

## Create the Network (old style)

TODO: This network is not currently being used.

This is the same as used while running.

![Setup your network](img/network-setup.png)

## Download the Debian netinst image

Go to the
[Debian download page](https://www.debian.org/download)
and locate the `debian-(VERSION)-amd64-netinst.iso` file.
The file is under 378 MB (for 11.2.0).
Save it in a suitable directory.  Note the checksums.

## Create the Machines

 1. Default memory size will do.  Use a 15 GB disk.  Add three machines:

      * `Client Desktop`, `Linux`, `Debian (64-bit)`
      * `Server Domain`,  `Linux`, `Debian (64-bit)`
      * `Client Domain`,  `Linux`, `Debian (64-bit)`

 2. Group the machines by selecting them together on the left hand,
    and make menu selection `Machine » Group`.  Then click the newly
    created group name and make menu selection `Group » Rename Group`.
    Set `InternetWide Realm Crossover` as the group name.

 3. Start each of the machines in turn by selecting them in the
    VirtualBox window and clicking the green `Start` arrow.
    Respond to the popup by providing the Debian netinst ISO
    with the folder-with-green-arrow button, and confirm with
    the `Start` button. 

      * In the installer, move down to `Install` and press Enter.
      * Enter on `English` and `English`.
      * Select `Other`, `Europe`, `Netherlands`.
      * Enter on `en_US.UTF-8` locale.
      * Enter on `American English` keyboard layout.
      * Wait.
      * Let `enp0s3` be the primary network interface.
      * As hostnames, enter `client-desktop`, `server-domain` and `client-domain` respectively.  As domains, enter TODO:NOT:`realm-crossover.demo.arpa2.org` BUT:`demo.arpa2.org`.
      * The `root` password is `xover4all`.
      * The user is `ARPA2` with account `arpa2` and password `arpa2all`.
      * Enter on `Guided Partitioning`, then `Use Entire Disk` and `SCSI3`
        and `All files in one partition` and `Finish and write to disk`.
        Walk to the `<Yes>` button to activate.
      * Wait.
      * Do not scan for more disks.
      * Enter a nearby mirror and, if you need it, an HTTP proxy.
      * We cannot distribute images that participate in package surveys.
      * To select software, toggle off `Desktop environment` and `Gnome`.
      * Enter to install GRUB; select the device offered.
      * Finally reboot into the new OS.

 4. The three will henceforth be connected by an internal network,
    while the first continues to connect to the normal Internet.
    Select each of the machines in turn, click the right-pane cogwheel
    button `Settings`, and click left on `Network` to make
    the settings shown below:

    ![Host network settings](img/host-network-2nd.png)

    The installer has not configured this interface, but we will
    do that later on.

 5. On the installed systems, login as `root` and install base software
    for building of our
    ["mkhere"](https://gitlab.com/arpa2/mkhere/-/blob/master/INSTALL.MD)
    packages, and install the container system
    ["mkroot"](https://gitlab.com/arpa2/mkroot).

      * Install `git`    with `apt-get install git`.
      * Install `gpm`    with `apt-get install gpm`.
      * Install `ccmake` with `apt-get install cmake-curses-gui`.
      * Install `tshark` with `apt-get install tshark`.
      * Clone "mkhere" with `git -C /    clone https://gitlab.com/arpa2/mkhere`
      * Clone "mkroot" with `git -C /opt clone https://gitlab.com/arpa2/mkroot`
      * Boostrap the build environment with `/mkhere/ospackages.sh ossetup`
      * Create a build directory with `mkdir /opt/mkroot/build`
      * Link "mkroot" to "mkhere" with `ln -s /mkhere /opt/mkroot/build/`
      * TODO:USELESS: Mount the Additions CD to `/media/cdrom` and run `sh VBoxLinuxAddtions.run`
      * You may want to use the Machine's File Manager to move this file to `/root` for copy/paste (using `gpm`)

 6. Instsall the `hosts` file from this repository to `/etc/hosts` on
    all three VMs.  The `/etc/resolv.conf` file should not need any
    work, as it is installed as part of DHCPv4 bootstrapping of the
    primary interface.  Also, the containers are configured by the
    "mkroot" process and are therefore aware of the fixed IPv6
    addresses on their local network.

    The `/etc/hosts` files work better when they have their own name
    attached after `127.0.0.1` so `client-domain`, `server-domain`,
    `client-desktop`.

 7. Setup for routing with `echo >> /etc/sysctl.conf "net.ipv6.conf.all.forwarding = 1"`

## Build the Client Desktop

The client desktop runs on network `fd52:d011:8590:f645::/64`.
The external interface will run at that address filled with zeroes.

Install `bridge-utils` so the network has a mostly similar shape
to the server machines.  Note that `/sbin/brctl` is used because
the graphical interface does not add `/sbin` to the `$PATH`,
not even for `root` and boot scripts.

```
# The secondary network interface
auto enp0s8
iface enp0s8 inet6 manual
	up	ip addr  add fd52:d011:8590:f645::/64 dev enp0s8
	down	ip addr  del fd52:d011:8590:f645::/64 dev enp0s8
	up	ip route add fdcc:1a68:53d2:6142::/64 dev enp0s8
	down	ip route del fdcc:1a68:53d2:6142::/64 dev enp0s8
	up	ip route add fd17:08c9:c09b:3b47::/64 dev enp0s8
	down	ip route del fd17:08c9:c09b:3b47::/64 dev enp0s8
```

### Needed Debian packages
```
# startup
apt install git build-essential cmake cmake-curses-gui
# arpa2common
apt install comerr-dev ragel doxygen graphviz liblmdb-dev libssl-dev
# quickder
apt install python3 python3-dev python3-setuptools
# Quick-SASL
apt install libsasl2-dev libsasl2-modules libsasl2-modules-db swig
# freediameter
apt install  gnutls-dev libsctp-dev libgcrypt-dev libidn11-dev bison flex 
# kip
apt install libkrb5-dev libkrb5-3 libunbound-dev sasl2-bin libev-dev libssl-dev libpcre3-dev libjson-c-dev libfcgi-dev bison flex python3-cbor
# firefox
apt install firefox-esr
# http_sasl_client
apt install python3-selenium curl libcurl4-openssl-dev qtbase5-dev qtdeclarative5-dev qml-module-qtquick2 qml-module-qtquick-controls2 qml-module-qtquick-layouts qml-module-qtquick-window2
# GNOME
apt install gnome-core gnome-tweak-tool
```

### Cloning
```
mkdir ~/arpa2
# arpa2cm
cd ~/arpa2
git clone https://gitlab.com/arpa2/arpa2cm.git
cd arpa2cm
mkdir build
cmake ..
# arpa2common
cd ~/arpa2
git clone https://gitlab.com/arpa2/arpa2common.git
cd arpa2common
mkdir build
cmake ..
# Quick-MEM
cd ~/arpa2
git clone https://gitlab.com/arpa2/Quick-MEM.git
cd Quick-MEM
mkdir build
cmake ..
# quickder
cd ~/arpa2
git clone https://gitlab.com/arpa2/quick-der.git
cd quick-der
mkdir build
cmake ..
# Quick-SASL
cd ~/arpa2
git clone  https://gitlab.com/arpa2/Quick-SASL.git
cd Quick-SASL
mkdir build
cmake ..
# freediameter
cd ~/arpa2
git clone  https://gitlab.com/arpa2/freediameter-idp.git
cd freediameter-idp
mkdir build
cmake ..
# kip
cd ~/arpa2
git clone https://gitlab.com/arpa2/kip.git
cd kip
mkdir build
cmake ..
# http_sasl_client
cd ~/arpa2
git clone https://gitlab.com/arpa2/http_sasl_client.git
cd http_sasl_client
mkdir build
cmake ..
```

### Installing
The next commands should be run as root
```
cd ~arpa2/arpa2
make -C arpa2cm/build install
make -C arpa2common/build install
make -C Quick-MEM/build install
make -C quick-der/build install
make -C Quick-SASL/build install
make -C freediameter-idp/build install
make -C kip/build install
```

### Firefox setup (as user arpa again)
#### Install native messaging host
```
mkdir -p ~/.mozilla
cp -r ~/arpa2/http_sasl_client/build/test/.mozilla/native-messaging-hosts ~/.mozilla
```

#### Install firefox Additions
In firefox press Ctrl-Shift-A, search for sasl-credentials and select "Add to firefox"

## Build the Server Domain

The server domain runs on network `fdcc:1a68:53d2:6142::/64`.
The external interface will run at that address filled with zeroes.

Install `serverdomain.sh` in `/` and have it run at boot time by
installing `serverdomain.service` in `/etc/systemd/system/` and
running `systemctl enable serverdomain.service`.

### Configuration files

On the guest, extend `/etc/network/interfaces` with
TODO: See included files

```
# The secondary network interface
auto enp0s8
iface enp0s8 inet6 manual
	up	ip addr  add fdcc:1a68:53d2:6142::/64 dev enp0s8
	down	ip addr  del fdcc:1a68:53d2:6142::/64 dev enp0s8
	up	ip route add fd17:08c9:c09b:3b47::/64 dev enp0s8
	down	ip route del fd17:08c9:c09b:3b47::/64 dev enp0s8
	up	ip route add fd52:d011:8590:f645::/64 dev enp0s8
	down	ip route del fd52:d011:8590:f645::/64 dev enp0s8
```

### Configure "mkroot"

Execute the "mkroot"
[setup instructions](https://gitlab.com/arpa2/mkroot/-/blob/master/doc/setup-internetwide-architecture-hosting.md)
in the build directory:

```
cd /opt/mkroot/build
ccmake ..
``

**TODO:** Avoid ending with a CA for `example.org` and remove `/opt/mkroot/build/ca` recursively if it is a problem, followed by a new run of `cmake`.

After configuring once, make the following settings:

  * `IWO_DOMAIN_ISP` to `unicorn.demo.arpa2.org`
  * `IWO_SCRAMBLER` to `demo123`
  * `SUPPRESS_BUILD_TESTING` may be set to `ON` for faster building

Configure this, and continue to set

  * `CONTRIB_WEB_APACHE` to `ON`
  * `DNS_PRIMARY_SLAVES` to `fd17:08c9:c09b:3b47::a2:53`
  * `DNS_SECONDARY_MASTER` to `fd17:08c9:c09b:3b47::a2:53`
  * `DNS_SECONDARY_ZONES to `pixie.demo.arpa2.org`
  * `IWO_PUBLIC_PREFIX64` to `fdcc:1a68:53d2:6142::`
  * `TLS_CONTROL_TOWER` to `OFF` (we use rather silly host names)

When done, generate the configuration to exit `ccmake`.  You may need to
reconfigure another time, if new variables appear at the top of the screen.

### Download and build "mkroot"

Then build the infrastructure with `make`.  It will take a long time, during
which our software stack is built from source, and any operating system
dependencies are also installed.

The result of the build is a collection of
[Open Containers](https://opencontainers.org/)
with our logic that can be run simply using
[RunC](https://github.com/opencontainers/runc).
Open Containers are the most likely follow-up to Docker,
and is supported by many large market players.

### Start "mkroot" containers

Now prepare for these containers running inside this VM, and let's use
Tmux to hold the various consoles for the containers.

  * Install RunC with `apt-get install runc`
  * Install `brctl` with `apt-get install bridge-utils`
  * Install Tmux with `apt-get install tmux`
  * Configure Tmux with `echo >> /etc/tmux.conf set-option -g prefix C-]`
    so it will use `^]` as its escape sequence instead of the `^B` default.
  * Change to the router with `cd /opt/mkroot/build/internetwide/pub2dmz`
  * Copy the network configuration with `cp debian.interfaces /etc/network/interfaces.d/`
  * Edit `/etc/network/interfaces.d/debian.interfaces` and comment out the `ip -4` lines and make sure `iwo0pub` looks like

    ```
    allow-hotplug iwo0pub
    iface iwo0pub inet6 manual
    	pre-up		ip link add iwo0pub type veth peer name pub0pub
    	post-down	ip link del iwo0pub
    	up	ip addr  add fdcc:1a68:53d2:6142::2/127 dev iwo0pub
    	down	ip addr  del fdcc:1a68:53d2:6142::2/127 dev iwo0pub
    	up	ip route add fdcc:1a68:53d2:6142::/64 via fdcc:1a68:53d2:6142::3 dev iwo0pub
    	down	ip route del fdcc:1a68:53d2:6142::/64 via fdcc:1a68:53d2:6142::3 dev iwo0pub
    ```

  * Reload the network with `systemctl restart networking`
  * Run `ifup iwo0pub` and TODO:IGNORE see it fail
  * Create the network with `./net4pub.sh create`
  * TODO: Create the TODO: tunnel
  * TODO:FIREWALL?
  * TODO: `pub2dmz` requires `root` user, `adm` group
  * TODO: `pub2dmz` wants to write in `/var/spool/rsyslog`, nex, ro
  * TODO: `pub2dmz` lacks permission to create/write `/var/log/daemon.log`
  * Start the router container with `runc run pub2dmz`

The services in the containers are taken from the Debian operating
system, so we want to avoid running those:

```
systemctl disable apache2 slapd krb5-admin-server krb5-kdc
```


## Build the Client Domain

The client domain runs on network `fd17:08c9:c09b:3b47::/64`.
The external interface will run at that address filled with zeroes.

### Configuration files

On the guest, extend `/etc/network/interfaces` with

```
# The secondary network interface
auto enp0s8
iface enp0s8 inet6 manual
	up	ip addr  add fd17:08c9:c09b:3b47::/64 dev enp0s8
	down	ip addr  del fd17:08c9:c09b:3b47::/64 dev enp0s8
	up	ip route add fdcc:1a68:53d2:6142::/64 dev enp0s8
	down	ip route del fdcc:1a68:53d2:6142::/64 dev enp0s8
	up	ip route add fd52:d011:8590:f645::/64 dev enp0s8
	down	ip route del fd52:d011:8590:f645::/64 dev enp0s8
```

TODO

### Configure "mkroot"

### Download and build "mkroot"

### Start "mkroot" containers

Install `clientdomain.sh` in `/` and have it run at boot time by
installing `clientdomain.service` in `/etc/systemd/system/` and
running `systemctl enable clientdomain.service`.

### TODO: mkroot

The services in the containers are taken from the Debian operating
system, so we want to avoid running those:

## Export VMs (old style)

TODO: Old style, with individual hosts, and possibly excluding networking.

In the VirtualBox window, select each of the machines in turn
and export them via the pop-up menu `Export to OCI`.  Use the
following settings:

  * For `Client Desktop`, replace the space by underscore to form `Client_Desktop.ova`
  * For `Server Domain`, replace the space by underscore to form `Client_Domain.ova`
  * For `Client Domain`, replace the space by underscore to form `Server_Domain.ova`
  * For each, select `Strip all network adapter MAC addresses`

## Export VMs

You may save a lot of bandwidth and storage space by compressing first.

### Compress first

**Hard links.**
While building the "mkroot" environments with "mkhere", many files
get copied into the `rootfs` directories from which the containers
run.  They can easily be turned into hard links if you run the
`dums-crush.sh` script from the `/opt/mkroot/build` directory.
The script iterates over the containers and tries to replace any
matching file in the containers with a file in the "mkhere"
environment.  The script may complain about `/dev/null`, which is
safe to ignore.

**Stop build.**
You are no longer building, and you have copied the relevant parts
to the relevant "mkroot" containers.  Now, you can stop building
with `rm -rf /dl /src /build /tree /mkhere` which will work even though
you just made links against this.

**Compress.**
Inside the container, run

```
dd if=/dev/zero of=/zarro bs=4096
rm -f /zarro
```

then, stop the container and outside of it run

```
IMAGE=/path/to/image.vdi
du -m $IMAGE
vboxmanage modifyhd $IMAGE --compact
du -m $IMAGE
```

### Actual Export

In the VirtualBox window, make the menu selection `File » Export Appliance`
and select the three hosts in the `InternetWide Realm Crossover` group:

  * `Client Desktop`
  * `Server Domain`
  * `Client Domain`

Make sure that these are grouped together, under a group named
`InternetWide Realm Crossover`.  This helps users to distinguish
the rather general names for the hosts from those in other projects;
it allows those users to keep the VMs some time without confusion.

In the next window:

  * Set the filename to `DemoRealmCrossover.ova` (in a suitable directory)
  * Select `Strip all network adapter MAC addresses`
  * Switch off the toggle `Write Manifest File`

## Test Import

You could now remove the three VMs and import them again:

  * Select all three VMs
  * Use the popup menu to select `Remove`
  * Confirm with the `Delete all files` button

Now you can import from scratch:

  * Use the menu selection `File » Import Appliance`
  * Click the folder-with-arrow button to select `DemoRealmCrossover.ova`
  * Select the `MAC Address Policy` of `Generate new MAC address for all network adaptors`
  * Confirm with the `Import` button

