# Running the SASL demo

> *We shall now set you up with three VMs that showcase
> Realm Crossover for SASL, featuring HTTP-SASL, for a
> client using a foreign webserver with an identity that
> is assure by the client domain.*

## Be sure to have VirtualBox

There are so many virtualisation tools... but this one
comes with a nice GUI, and is pretty much self-contained.

You should find VirtualBox in your Linux distribution.
For Windows and Mac OS X,
[download VirtualBox](https://www.virtualbox.org/)
and do what you usually do.

These instructions were written for VirtualBox 6.1 on
Ubuntu 20 Focal Fossa (Linux), but should work in most other
variations just as well.

## Download the demo VMs (old style)

TODO: This uses the old style with separate hosts

The following demonstration images are needed:

  * [Client_Desktop.ova](javascript:alert('To be done')) entitled `Client Desktop`
  * [Server_Domain.ova](javascript:alert('To be done')) entitled `Server Domain`
  * [Client_Domain.ova](javascript:alert('To be done')) entitled `Client Domain`

## Download the demo VMs

There are 3 demo VMs, combined in an appliance file named
`DemoRealmCrossover.ova`.  You can
[download the appliance file](javascript:alert('To be done'))
and save it in a suitable directory.

## Create a Network (old style)

TODO: The new style includes this in the appliance file.

We are using a network that connects the VMs.  This is
what would normally be the Internet.  Since we don't know
if you have IPv6 and SCTP and can spare the /64 prefix for
a normal server deployment, we will spare you the worry
and contain all traffic on this virutal network.

  * In the VirtualBox menu choose `File » Preferences`
    and select `Network`.  On the right, click the top
    button that `Adds new NAT network`.

  * Doubleclick the newly created network and edit the
    fields to look like this:

    ![Setup an internal IPv6 network](img/network-setup.png)

## Create the Machines

Import the appliance file:

  * Use the menu selection `File » Import Appliance`
  * Click the folder-with-arrow button to select `DemoRealmCrossover.ova`
  * Select the `MAC Address Policy` of `Generate new MAC address for all network adaptors`
  * Confirm with the `Import` button

Group the machines by selecting them together on the left hand,
and make menu selection `Machine » Group`.  Then click the newly
created group name and make menu selection `Group » Rename Group`.
Set `InternetWide Realm Crossover` as the group name.

