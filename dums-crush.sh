#!/bin/sh
#
# Crush the "mkhere" and "mkroot" structures after building
#
# This script is to be run in the "mkroot" build directory, and it follows the
# "mkhere" link from there.  It finds matching paths in the rootfs directories
# and, when the files are the same, they will be hard-linked to the one in
# the "mkhere" directory.
#
# This script is idempotent, meaning you can run it again, or in parts.
# The changes are atomic, meaning you will not be left with missing files.
#
# From: Rick van Rein <rick@openfortress.nl>


for ROOTFS in */*/rootfs
do
	echo "ENTER $ROOTFS"
	( cd "$ROOTFS" ; find * -type f ) | \
	while read FILENAME
	do
		#DEBUG# echo "CHECK $FILENAME"
		for MADEFILE in mkhere/../tree/*/"$FILENAME" "/$FILENAME"
		do
			#DEBUG# echo "AGAINST $MADEFILE"
			if cmp -s "$ROOTFS/$FILENAME" "$MADEFILE"
			then
				# echo WOULD rm "$ROOTFS/$FILENAME"
				# echo WOULD ln "$MADEFILE" "$ROOTFS/$FILENAME"
				# rm "$ROOTFS/$FILENAME"
				ln -f "$MADEFILE" "$ROOTFS/$FILENAME"
				#DEBUG# ls -li "$ROOTFS/$FILENAME"
				#DEBUG# ls -li "$MADEFILE"
				break
			fi
		done
	done
done

