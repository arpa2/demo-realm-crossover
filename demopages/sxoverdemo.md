<p align="right"><small>[InternetWide](http://internetwide.org) » [Demo](index.html) » [Realm Crossover](sxoverdemo.html)</small></p>

# Demonstration of Realm Crossover for SASL

> *Realm Crossover passes authentication from a relying-service domain to an
> identity provider representing the client's domain.  We demonstrate how
> this can be done by relaying SASL traffic over Diameter.*

The use of HTTP-SASL makes it easy to pass authentication traffic into a
backend.  This backend can be easily standardised and even relayed to an
identity provider that runs under the client's own control.  This enables
service providers to offer clients to Bring Your Own IDentity, instead
of yet another account at yet another website after yet another email
"authentication" procedure.

**Take note** that we are protocol desigers, not GUI designers.  The popup
may be a bit clunky, and is mostly to show possibilities.
Do not close the popup yourself.  It is only started when FireFox starts.
Should it crash, then stop and restart FireFox to get a new login demo.


## Run the Demo

Proceed as follows:

  * Open the
    [HTTP-SASL demo page](https://web0.unicorn.demo.arpa2.org/demo/http-sasl/)
    in a separate tab
  * Authenticate as user `demo` and password `sekreet`.  Set the SASL
    mechanism to `SXOVER-PLUS`.  Your domain `pixie.demo.arpa2.org` will
    be used to find your identity provider.
  * After login, your authenticated user identity is shown to you.
    As you can see, it involves the `pixie.demo.arpa2.org` domain
    in which your identity provider runs.
  * In this procedure you managed to Bring Your Own IDentity (BYOID).

What this shows you, is that we always use the form `user@domain.name` for
an authenticated user and, given that you run an identity provider, you can
you can login under your own domain.  This is what your selection for the
`SXOVER-PLUS` mechanism demonstrates.

Please compare this situation with the HTTP-SASL case.  The vital difference
is the domain name, which is the start of authority over online conduct.


## Background

Users of the Internet have been reduced from first-class citizens to mere
*click bait* under the pretense of bringing them "free" services.
Users do not even control their own identity, in contrast with the intentions
of the Internet to harbour a domain name for anyone as a sovereign entry into
the online World.

Owning a domain however, involves running services.  This is not expensive,
but it requires technical abilities that are not common.  Hosting providers form
a stepping stone, but they are struggling to keep up with integrated packages
pushed out by a few very large actors.  Open source offers alternatives, but
is not very good at integration.

The approach sketched on the [hacking page](hacking.html) describes our work
on the *InternetWide Architecture* for better integration with containers that
connect to remote containers almost as easily as to local ones.  The suspicion
is that some hosting providers become Identity Providers (under control of
their customers) while others become Plugin Service Providers who specialise on
one or a few specific components.  Customers can select such services from
external parties and plug those into their domain name.

We wrote a
[Domain Owner's Manual](https://gitlab.com/arpa2/domain-owners-manual)
as an evolving document to which we want to add things that empower
users to return to their fair state of first-class citizens.

All this builds on a shared idea of Identity, Authentication and Access Control.
It should be no surprise then, that we worked on all that:

  * [Identity with Selectors](http://common.arpa2.net/md_doc_IDENTITY.html)
    play a defining role for
    [InternetWide Realm Crossover](https://datatracker.ietf.org/doc/html/draft-vanrein-internetwide-realm-crossover).
    The Identity concepts support advanced features such as
    [Groups](http://common.arpa2.net/md_doc_GROUP.html)
    and
    [Actors](http://common.arpa2.net/md_doc_ACTOR.html).
  * [Access Control for Communication](http://common.arpa2.net/md_doc_ACCESS_COMM.html)
    and [Documents](http://common.arpa2.net/md_doc_ACCESS_DOCUMENT.html), for starters.
  * [Visit our blog](http://internetwide.org/) for more background.


## What is Realm Crossover?

The idea of Realm Crossover is shown below.

![*Client Desktop connects to Server Domain, which authenticates the client via a callback to the Client Domain.*](cli-srv-cli.svg)

We all play the role of client when we access a server hosted somewhere.
These servers usually ask us to login, so they know what context to show
to us.  Instead of an account with every single server, and loosing
passwords or forgetting account names, we can use Real Crossover.

This starts with the client stating that they want to login using their
own hosted domain, shown as the Client Domain in the picture.  The
Server Domain connects to the Client Domain to obtain a user identity.
It then attaches the domain name of the client to form something
of the form `userid@client.domain` and treats that string as the
authenticated user identity.

It is up to the Client Domain to handle user identities and to state
them for the right clients.  To that end, the client login is passed
through the Server Domain to the Client Domain, where it is validated.
The entire flow is inivisible to the Server Domain, with the exception
of the `client.domain` name that indicates where to connect.


## What is the InternetWide Architecture?

The purpose of the
[InternetWide Architecture](http://internetwide.org/tag/architecture.html)
is to turn users into first-class citizens of the Internet.
This starts by controlling their own
[identities](http://internetwide.org/tag/identity.html),
underneath their own Client Domain, and to
[Bring Your Own IDentity](http://internetwide.org/blog/2015/04/22/id-2-byoid.html)
to any Server Domain.

In general, we provide software for a
[hosting infrastructure](http://internetwide.org/tag/hosting.html)
for
[domain names](https://gitlab.com/arpa2/domain-owners-manual)
that is designed to
run on anything from a small home system to a large hosting facility
in some co-located server machine.  Note that we are careful not to
use vague terms like *cloud*, but if that is how you host your domain
then it should work just as well.  We just prefer to point more
accurately at components than at a haze in the sky.


## Software Used

Our web software demonstrated here is:

  * [internetwide/web](https://gitlab.com/arpa2/mkroot/-/tree/master/internetwide/mail)
    is our web server, built into an Open Container.  It uses a FastCGI backend.
  * [mod_arpa2_diasasl.so](https://gitlab.com/arpa2/apachemod/-/tree/master/arpa2_diasasl)
    is an Apache plugin for our
    [Quick-DiaSASL protocol](https://gitlab.com/arpa2/apachemod/-/tree/master/arpa2_diasasl).
  * [internetwide/lanservice](https://gitlab.com/arpa2/mkroot/-/tree/master/internetwide/lanservice/)
    concentrates the Quick-DiaSASL protocol in
    [freediameter-sasl](https://gitlab.com/arpa2/freediameter-sasl)
    whose client side handles the login.
  * [internetwide/identity](https://gitlab.com/arpa2/mkroot/-/tree/master/internetwide/identity/)
    receives foreign services' attempts to authenticate visitors with SASL
    under Realm Crossover.


