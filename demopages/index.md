<p align="right"><small>[InternetWide](http://internetwide.org) » [Demo](index.html)</small></p>

# Demo for InternetWide Realm Crossover

> *Welcome.  This is a demo that shows work that we have done,
> quietly in the background, to fix central problems about the
> way the Internet works.  Control over Identity and more focus
> on hosting a variety of integrating services is central.*

![*Client Desktop connects to Server Domain, which authenticates the client via a callback to the Client Domain.*](cli-srv-cli.svg)


## Running the Demo's

*Presuming you Downloaded and Installed as described below...*

You should start all three VMs and watch the Client Desktop.
It will log you on as user `arpa2` and present you with a FireFox
browser and a login popup.  *Do not close the popup dialog*, but
leave it be.  You may otherwise end up having to restart FireFox.

*If that was already done, continue below.*

You can always return to this page with the browser's `Home` button.
The following options are now available:

  * [Demonstration of HTTP-SASL](httpsasldemo.html)
  * [Demonstration of Realm Crossover for SASL](sxoverdemo.html)
  * [Demonstration of Mail Addresses with Access Control](maildemo.html)
  * [Hacking this Demo](hacking.html)


## Downloading or Upgrading the Demo

This demonstration and possibly later versions
can be downloaded from our
[RSync server](rsync://rsync.internetwide.org/InternetWide/)
&mdash; the image is large, because it delivers a complete
environment with two domain hosting environments.

If you received the demo on a USB Stick then you're all set,
but using RSync may make it fast to recover from corruption
or mistakes, as well as to upgrade to future releases of the
Demo.

*You are welcome to redistribute the USB Stick or otherwise
spread the word about our work.*


## Installing the Demo

This demo can be run inside
[VirtualBox](https://www.virtualbox.org/).
Install the downloaded file with `File » Import Appliance` and
wait until it has loaded.  Then, you should have three Virtual
Machines (VMs) in a group named `InternetWide Realm Crossover`,

  * `Client Desktop` is a VM with a graphical user interface.
    You can start it and a FireFox browser should appear with
    this document as its home page.  We extended FireFox with
    support for
    [HTTP-SASL](https://www.ietf.org/id/draft-vanrein-httpauth-sasl-06.html)
    and there is a connection to a desktop popup that currently
    supports password entry, but that could also be extended to
    support Kerberos for single-signon.

  * `Server Domain` is a VM that represents the hosted domain
    for a web server.  The web server has a plugin module for
    HTTP-SASL, which passes over a simple protocol called
    [Quick-DiaSASL](https://tools.ietf.org/id/draft-vanrein-diameter-sasl-06.html#name-centralised-handing-of-sasl)
    to reach a local Diameter client that passes
    [SASL over Diameter](https://tools.ietf.org/id/draft-vanrein-diameter-sasl-06.html#name-avp-definitions-for-sasl-in)
    to the `client.domain` name found in a login attempt.

  * `Client Domain` is a VM that represents the hosted domain
    that defines the client identity.  The identity provider
    accepts
    [SASL over Diameter](https://tools.ietf.org/id/draft-vanrein-diameter-sasl-06.html#name-avp-definitions-for-sasl-in)
    and provides the local `userid` that the web server can
    then compose with `client.domain` to form `userid@client.domain`.

Each VM is a box in the diagram above, and normally would be on a
different part of the network.  The VMs are connected directly
via an internal network `xoverdemo` via their second adapters
while their primary adapters share your machine network via NAT.

Although you will initially use the Client Desktop only, you need
the three components for the demo.  You can start each in turn with
a double click or using the `Start` button on them.  If you have a
technical mindset, you will probably appreciate zooming in on the
Server Domain and Client Domain later on.  We installed `wireshark`
on the Client Desktop and `tshark` on the Client Domain and Server
Domain so you can tap the secondary `enp0s8` interface adapter.


## Thanks for Support

Our work on the InternetWide Architecture takes great effort, and
has not been able without support by a few parties who subscribe
to the underlying purposes and trusted our team to work on it.
In order of appearance, these have been:

  * [NLnet](https://nlnet.nl/) is a sponsor of open source
    software development, and subscribes to the idea that
    users should be central in an Internet, and that this is
    increasingly deteriorating.

  * [SIDNfonds](https://sidnfonds.nl/) also works to improve
    the Internet from varying angles, and has a strong backing
    of hosting providers.

  * [NGI Pointer](https://pointer.ngi.eu/) is part of the EU's
    work towards a Next Generation Internet, embracing the
    European take on privacy and user rights.  The specifics
    of the Pointer program are geared towards sponsoring new
    ideas that may prove positively disruptive for the
    architecture of the Internet.

*Thanks for your support of our ambituous project!*

