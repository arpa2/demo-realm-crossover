<p align="right"><small>[InternetWide](http://internetwide.org) » [Demo](index.html) » [HTTP-SASL](httpsasldemo.html)</small></p>

# Demonstration of HTTP-SASL

> *Most protocols incorporate the SASL framework, which independently develops
> authentication.  HTTP does not, which has caused application-level logic to
> take over.  HTTP-SASL adds this better habit of other protocols into the web.*

This is not a demonstration of Realm Crossover, but it uses HTTP-SASL with
local accounts.  Actually, we use the same mechanism as for Realm Crossover
by passing SASL to an identity provider; it just happens to be a local one.

**Take note** that we are protocol desigers, not GUI designers.  The popup
may be a bit clunky, and is mostly to show possibilities.
Do not close the popup yourself.  It is only started when FireFox starts.
Should it crash, then stop and restart FireFox to get a new login demo.


## Run the Demo

Proceed as follows:

  * Open the
    [HTTP-SASL demo page](https://web0.unicorn.demo.arpa2.org/demo/http-sasl/)
    in a separate tab
  * Authenticate as user `demo` and password `sekreet`.  Set the SASL
    mechanism to `DIGEST-MD5`.  Your domain `pixie.demo.arpa2.org` may
    be set, but it will be ignored for `DIGEST-MD5` authentication.
  * After login, your authenticated user identity is shown to you.
    As you can see, it involves the `unicorn.demo.arpa2.org` domain
    in which the server runs.

What this shows you, is that we always use the form `user@domain.name` for
an authenticated user and, given that you have an account on the server,
you can login under that domain.  This is what your selection for the
`DIGEST-MD5` mechanism demonstrates.

Realm Crossover is the mature variation on this scheme, but it is not
enforced.  Both the server and you have a choice when they use HTTP-SASL.
If the server stops offering local accounts however, you can switch to
the `SXOVER-PLUS` mechanism for Realm Crossover.


## Native Support

The popup window used for authentication is a native application.
FireFox is the first to implement a new standard for making such connections
to a tightly/securely coupled desktop application.  And our work on HTTP-SASL
may well be the first to use it.

What this means is that your credentials are handled outside of the
browser environments.  Notably, out of reach of JavaScript.  What passes
over the wire is solely determined by the SASL mechanisms that you choose.
Plain passwords might still be picked up by rogue elements in a browser,
but `DIGEST-MD5` is a bit better, and `SXOVER-PLUS` is strongly protected.

Our aim for the future is to incorporate Kerberos authentication as well.
This is only available on the desktop.  This form of authentication is
old, but highly potent in a scary environment such as today's Internet or,
for that matter, a browser that downloads scripts from anywhere.  The
benefit of Kerberos is that it is fast, quantum-proof and, very pleasant
for users, it is a single-signon system.


## Software Used

Our web software demonstrated here is:

  * [internetwide/web](https://gitlab.com/arpa2/mkroot/-/tree/master/internetwide/mail)
    is our web server, built into an Open Container.  It uses a FastCGI backend.
  * [mod_arpa2_diasasl.so](https://gitlab.com/arpa2/apachemod/-/tree/master/arpa2_diasasl)
    is an Apache plugin for our
    [Quick-DiaSASL protocol](https://gitlab.com/arpa2/apachemod/-/tree/master/arpa2_diasasl).
  * [internetwide/lanservice](https://gitlab.com/arpa2/mkroot/-/tree/master/internetwide/lanservice/)
    concentrates the Quick-DiaSASL protocol in
    [freediameter-sasl](https://gitlab.com/arpa2/freediameter-sasl)
    whose client side handles the login.


