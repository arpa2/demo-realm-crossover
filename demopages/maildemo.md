<p align="right"><small>[InternetWide](http://internetwide.org) » [Demo](index.html) » [Mail with Access Control](maildemo.html)</small></p>

# Demonstration of Mail Addresses with Access Control

> *We can use Access Control to constrain who may send email to a recipient.
> The recipient crafts a special address for that purpose, involving a signature.
> This is something only the recipient can do, so it requires authentication.*


This is not a demonstration of Realm Crossover, but it uses HTTP-SASL to login
on the web server in the Client Domain.  This is enforced over a FastCGI plugin
that talks to the mail server.


## Run the Demo

Proceed as follows:

  * Open the
    [Makemail utility](https://web0.pixie.demo.arpa2.org/internetwide/mail/makemail.cgi)
    in a separate tab
  * Authenticate as before, user `demo` and password `sekreet`.
    You can use SASL mechanism `DIGEST-MD5` or `SXOVER-PLUS`.
  * Allow connections from the `example.com` domain name.
  * Clear the timeout or change it, as you like.
  * Click the ` MakeMail » ` button.
  * Authenticate once more.
  * You can use the ` Copy » ` button to clone the address.  Keep the page open.

Now you can open a Terminal (under Activities) and manually connect to the
mail server in the Client Domain.  This gets a bit technical, as you will now
be doing the SMTP dance to see how mail servers communicate.

  * In a terminal, run `nc mail0.pixie.demo.arpa2.org 25` to connect over SMTP.
  * The mail server greets you with `220 mail0.pixie.demo.arpa2.org ESMTP Postfix (Debian/GNU)`
  * Return the pleasantries with `EHLO example.com` (or `EHLO example.net`)
  * The server responds with a list of features and ends with `250 ...` without dash.
  * You now enter `MAIL FROM:<john@exmaple.com>` if you want to match the email address constraints,
    or otherwise something like `MAIL FROM:<mary@example.net>` which clearly has another domain.
  * The server agrees with `250 2.1.0 Ok` because it is not yet aware of who you intend to send to.
  * You now enter `RCPT TO:<` followed by the pasted email address and `>` all as one line.
  * If you came from the permitted domain, you will see `250 2.1.5 Ok`.
  * If you came from another domain, you will see `553 Recipient address enforces another context`

What this shows you, if you run both variants on the mail server, is that it
can distinguish between the sender domains and permit only the desired one to pass.
If you set an expiration in a few days, you could try again later to see that also
being imposed.

The purpose here is to stay in control when others spread your email address,
possibly in disregard for privacy laws or your own feeling of who may have it.
(In a complete setup, there would be an Access Control setting to disallow the
use of an unsigned address, but this demo does not enforce that.)

This protection would be a fluke if the page for constructing these signed addresses
was publicly availabel.  And so, we use authentication.  The reason you had to login
twice is because we give you authentication as-is.  Practical software remembers a
recent success of login, so it does not require login for every page.


## Software Used

Our mail software demonstrated here is:

  * [internetwide/mail](https://gitlab.com/arpa2/mkroot/-/tree/master/internetwide/mail)
    is our incoming mail server, built into an Open Container.
  * [AxeSMTP](https://gitlab.com/arpa2/AxeSMTP)
    is the mail server toolkit and it also contributes the `makemail.fcgi` FastCGI program.
  * [ARPA2 Common](https://gitlab.com/arpa2/arpa2common)
    with its
    [online documentation](http://common.arpa2.net/)
    provide the generic ARPA2 Identity and Access Control mechanisms.

Our web software demonstrated here is:

  * [internetwide/web](https://gitlab.com/arpa2/mkroot/-/tree/master/internetwide/mail)
    is our web server, built into an Open Container.  It uses a FastCGI backend.
  * [mod_arpa2_diasasl.so](https://gitlab.com/arpa2/apachemod/-/tree/master/arpa2_diasasl)
    is an Apache plugin for our
    [Quick-DiaSASL protocol](https://gitlab.com/arpa2/apachemod/-/tree/master/arpa2_diasasl).
  * [internetwide/lanservice](https://gitlab.com/arpa2/mkroot/-/tree/master/internetwide/lanservice/)
    concentrates the Quick-DiaSASL protocol in
    [freediameter-sasl](https://gitlab.com/arpa2/freediameter-sasl)
    whose client side handles the login.


## Posing a Challenge

Would you like to play around with HTTP-SASL and perhaps Realm Crossover?  Then interfacing
with this utility can be a good start.  When asked for `Access: application/json` it will
happily produce less wordy content, and even just `null` in response to failures *after*
authentication succeeded.

To support you with this kind of thinking, we delivered a complete hosting environment
that gives you the freedom to [hack around](hacking.html) as you desire.  One thing
you might like to do, is view the console of the `mail0` container in the Client Domain,
because it currently dumps the signatures contained in the email adress while verifying them.


## Demonstration Screenshot

![Mail Demo in Motion](maildemo.png)

*The multiple title bars on top are from the i3 window manager, navigating the
VirtualBox screens in "stacked" mode.  Using the modifier key with up/down arrows, this
setup can switch quickly between screens.   Unlike on the Gnome desktop, the mouse is
reduced to an optional extra, so it does not slow down repetitious context switching.*


