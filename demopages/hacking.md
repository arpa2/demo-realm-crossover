<p align="right"><small>[InternetWide](http://internetwide.org) » [Demo](index.html) » [Hacking](hacking.html)</small></p>

# Hacking this Demo

> *This is a desktop with two fully-featured domains.  Live out your technical heart.*

The Client Domain and Server Domain are similar in setup, but they have different
purposes in the demo.

  * The Client Domain is related to (possibly run by) the user on the Client Desktop.
  * The Server Domain is an external/foreign service that links back to the Client Domain
    for authentication under Realm Crossover.  This demo shows the SASL variant.


## Account Information

There are two accounts on each machine.  The first is for a normal user,

```
User: arpa2
Pass: arpa2all
```

The other is for system administration only,

```
User: root
Pass: xover4all
```


## Wire Tapping

We installed `wireshark` on the Client Desktop, and `tshark` on the Domains.
The network over which they connect is called `enp0s8` in Linux.
Not all traffic is TLS-encrypted yet.  We use `fdXX:XXXX:XXXX:XXXX::/64`
prefixes, which is a randomly generated local prefix.  Each VirtualBox VM
has its own prefix.

You will find many bridges on the Domains.  These connect a number of
containers that run for each Domain.  The most important one is `dmz0`
which carries traffic for the public prefix of the Domain.  Another is
`svc0` which concentrates traffic towards a LAN Service and `bck0`
that connects components more generally, such as a FastCGI plugin into
a web server.

After `ip netns list` you can see a number of network name spaces for
the containers below.  We generally like to do things like

```
ip netns exec svc0ns ss -S
ip netns exec web0ns tshark -S0 -w /tmp/client-web0.pcap -ni dmz0 tcp port 443
ip netns exec web0ns tshark -S0 -w /tmp/client-web0.pcap -ni bck0 tcp port 3000
```


## Special Protocols

We designed a few protocols that you may not be accustomed to.

  * SASL over Diameter is what the Domains use to relay authentication
    traffic during Realm Crossover.  It uses a few AVPs that carry a
    SASL token, mechanisms and channel binding information.  The `svc0`
    container running `internetwide/lanservice` in the Server Domain
    connects to the `id0` container running `internetwide/identity`
    in the Client Domain.

    [specification](https://datatracker.ietf.org/doc/html/draft-vanrein-diameter-sasl),
    [code](https://gitlab.com/arpa2/freediameter-sasl).

  * HTTP-SASL is an authentication extension for HTTP, that makes it
    inherit a lot of generally defined authentication mechanisms.  Most
    other protocols import such mechanisms via SASL, and HTTP is ready
    for it too, so we can push authentication down into the web server
    and not have to reinvent simplistic password mechanisms in every web
    application.

    [specification](https://datatracker.ietf.org/doc/html/draft-vanrein-httpauth-sasl),
    [desktop code](https://gitlab.com/arpa2/http_sasl_client/),
    [local-authentication plugin](https://gitlab.com/arpa2/apachemod/-/tree/master/arpa2_sasl),
    [realm-crossing plugin](https://gitlab.com/arpa2/apachemod/-/tree/master/arpa2_diasasl).

  * Quick-DiaSASL is a simple notation for SASL over Diameter, which is
    much easier for SASL authenticators.  This makes it light-weight to
    make modules that include Realm Crossover for SASL into services that
    now handle SASL authentication locally.  It is a simple ASN.1
    protocol and we even provide an API to make it easy on plugin
    writers, but it's so simple that we also built the simple structures
    directly, for instance to get a pure Python implementation.

    [specification](https://datatracker.ietf.org/doc/html/draft-vanrein-diameter-sasl-06#appendix-A),
    [authenticator plugin](https://gitlab.com/arpa2/apachemod/-/tree/master/arpa2_diasasl),
    [service concentrator](https://gitlab.com/arpa2/freediameter-sasl).

  * KIP is a mechanism for "push-mode encryption" that we use to wrap
    SASL traffic for the `SXOVER-PLUS` mechanism.  This is how we
    establish an end-to-end tunnel between the Client Desktop and the
    Client Domain, even when passing through the Server Domain.

    no specification yet,
    [code](https://gitlab.com/arpa2/kip/).


## Building Infrastructure

We built the Domains with our own light wrappers around CMake and RunC.

  * We used ["mkhere"](https://gitlab.com/arpa2/mkhere/)
    as a repository of build instructions.  Our own software is CMake, but we can also
    handle other build styles with this set of packages.  We also rely on the OS.

  * We used ["mkroot"](https://gitlab.com/arpa2/mkroot/)
    to build the
    [Open Containers](https://opencontainers.org/)
    that are subsequently run with RunC.

  * Each domain has a startup script in its root directory.  It starts containers
    with `runc`, using `tmux` to capture their console.  You can start with
    `tmux attach` and use the `Ctrl-]` key combination to operate it.


## Containers

The InternetWide Architecture puts great emphasis on domain hosting, and
facilitates it by building containers that address our concerns about
security and privacy.  Thanks to the building infrastructure, we could
showcase how that looks.

This demo uses a number of containers, which represent the logic of each Domain.
You can list them with `runc list` and find their consoles via `tmux attach`.

  * `pub0` runs the `internetwide/pub2dmz` component, which is basically our
    route to the outside World.  It allows an IPv6-only internal network even
    though the outside World continues to support IPv4.

  * `svc0` runs the `internetwide/lanservice` component, where local traffic
    concentrates, like DNS and Quick-DiaSASL.

  * `id0` runs the `internetwide/identity` component as an identity provider.
    In this demo, it is mostly used as the serving side for SASL over Diameter.

  * `kip0` runs the `internetwide/kip` component to supply encryption keys for
    push-mode encryption.  It has broad uses, but this demo only uses it for the
    `SXOVER-PLUS` mechanism, where it yields an end-to-end encryption tunnel.

  * `web0` runs the `internetwide/web` component as a web server running Apache.
    In this demo, it offers web pages from the `demo/http-sasl` component and
    connects to the `internetwide/mail` component over FastCGI.

  * `mail0` runs the `internetwide/mail` component as an incoming mail server.
    It does not actually relay email, but will drop emails for `demo` in the
    endless storage facilities at `/dev/null`.  In this demo, it is used to
    demonstrate Access Control based on a specially crafted email address.
    This address can be constructed over a FastCGI program `makemail.fcgi`.

Each container has its own network namespace with the matching name and `ns`
added, like `web0ns` for the web server component.


## Your Server Software

We would be thrilled if you considered adding Realm Crossover to your server
software.  Chances are that we made it really easy for you, by providing the
Quick-DiaSASL protocol and the `svc0` setup of a Diameter service.

This demo may also be helpful as a development environment.  It has the
customary development tools integrated.

We would love to hear from you if you embark on this path!

*Good luck!*
